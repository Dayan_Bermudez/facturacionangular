import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ClienteComponent } from './cliente/cliente.component';
import { UsuarioComponent } from './usuario/usuario.component';
import { ProductoComponent } from './producto/producto.component';
import { HomeComponent } from './home/home.component';
import { DialogClienteComponent } from './cliente/dialog/dialogcliente.component';
import { DialogProductoComponent } from './producto/dialog/dialogproducto.component';
import { DialogusuarioComponent } from './usuario/dialogUpdate/dialogusuario.component';
import { DialogDeleteComponent } from './common/delete/dialogdelete.component';
import { FacturacionComponent } from './facturacion/facturacion.component';
import { DialogFacturacionComponent } from './facturacion/dialog/dialogfacturacion.component';
import { DialogCreateusuarioComponent } from './usuario/dialogCreate/dialog-createusuario/dialog-createusuario.component';


import { HttpClientModule } from "@angular/common/http";
import { interceptorProvider } from './security/interceptors/prod-interceptor.service';

import { MatToolbarModule } from "@angular/material/toolbar";
import { MatTableModule } from "@angular/material/table";
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from "@angular/material/dialog";
import { MatInputModule } from "@angular/material/input";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatExpansionModule } from '@angular/material/expansion';
import { DialoghomeComponent } from './home/dialog/dialoghome/dialoghome.component';
import { LoginComponent } from './auth/login/login.component';
import { MatCardModule } from "@angular/material/card";
import { MatTabsModule } from "@angular/material/tabs";
import { MenuComponent } from './menu/menu/menu.component';


@NgModule({
  declarations: [
    AppComponent,
    ClienteComponent,
    UsuarioComponent,
    ProductoComponent,
    HomeComponent,
    DialogClienteComponent,
    DialogProductoComponent,
    DialogusuarioComponent,
    DialogDeleteComponent,
    FacturacionComponent,
    DialogFacturacionComponent,
    DialogCreateusuarioComponent,
    DialoghomeComponent,
    LoginComponent,
    MenuComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule, 
    HttpClientModule,
    MatTableModule,
    MatButtonModule,
    MatDialogModule,
    MatInputModule,
    MatSnackBarModule,
    FormsModule,
    ReactiveFormsModule,
    MatExpansionModule,
    MatCardModule,
    MatTabsModule
  ],
  providers: [interceptorProvider],
  bootstrap: [AppComponent]
})
export class AppModule { }
