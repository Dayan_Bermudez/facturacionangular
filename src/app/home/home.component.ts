import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { TokenService } from '../security/services/token.service';
import { PdfService } from '../services/pdf.service';
import { DialoghomeComponent } from './dialog/dialoghome/dialoghome.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public isLogged = false;
  public nombreUsuario = '';

  public documento:any;
  public algo: any;
  constructor(
    private pdfService: PdfService,
    private dialog: MatDialog,
    private tokenService: TokenService
  ) { }

  ngOnInit(): void {
    //this.get();
    if(this.tokenService.getToken()!='undefined'){
      this.isLogged = true;
      this.nombreUsuario = this.tokenService.getUsername();
    }
  }

  get() {
    this.pdfService.getPDF().subscribe(
      response => {
        this.algo = response.data;
        this.documento = document.createElement('object'); 
        this.documento.style.width = '100%';
        this.documento.style.height = '842pt';
        this.documento.type = 'application/pdf';
        this.documento.data = 'data:application/pdf;base64,' + [response.data];
        document.body.appendChild(this.documento);
      }
    );
  }

  openDialog(documento: any){
    const dialogRef = this.dialog.open(DialoghomeComponent,{
      data:documento,
      width: '90%',
      height: '90%'
    });
  }

}
