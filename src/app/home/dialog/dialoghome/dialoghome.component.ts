import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Respuesta } from 'src/app/model/respuesta';

@Component({
  selector: 'app-dialoghome',
  templateUrl: './dialoghome.component.html',
  styleUrls: ['./dialoghome.component.scss']
})
export class DialoghomeComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public documento: Respuesta,
    public dialogRef: MatDialogRef<DialoghomeComponent>
  ) { }

  ngOnInit(): void {
    this.print();
  }

  cerrarDialog(){
    this.dialogRef.close();
  }

  print(){
    document.body.append(this.documento.data);
  }

}
