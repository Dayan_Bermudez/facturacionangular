import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Respuesta } from 'src/app/model/respuesta';
import { JwtDto } from '../model/jwtdto';
import { LoginUsuario } from '../model/loginusuario';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  url: string = 'http://localhost:8080/auth/'

  constructor(
    private _http: HttpClient
  ) { }

  public login(loginUsuario: LoginUsuario):Observable<Respuesta>{
    return this._http.post<Respuesta>(this.url+'login',loginUsuario);
  }
}
