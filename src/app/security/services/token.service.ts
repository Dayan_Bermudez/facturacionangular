import { Injectable } from '@angular/core';

const TOKEN_KEY = 'AuthToken';
const USERNAME_KEY = 'AuthUsername';
const AUTHORITIES_KEY = 'AuthAuthorities';

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  rol: string='';

  constructor() { }

  public setToken(token:string):void {
    window.sessionStorage.removeItem(TOKEN_KEY);
    window.sessionStorage.setItem(TOKEN_KEY,token);
  }

  public getToken():string {
    let token_key = sessionStorage.getItem(TOKEN_KEY);
    return (token_key!=null ? token_key : 'undefined');
  }

  public setUserName(userName:string):void {
    window.sessionStorage.removeItem(USERNAME_KEY);
    window.sessionStorage.setItem(USERNAME_KEY,userName);
  }

  public getUsername():string {
    let username_key = sessionStorage.getItem(USERNAME_KEY);
    return (username_key!=null ? username_key : 'undefined');
  }

  public setAuthorities(authorities:string):void {
    window.sessionStorage.removeItem(AUTHORITIES_KEY);
    window.sessionStorage.setItem(AUTHORITIES_KEY,JSON.stringify(authorities));
  }

  public getAuthorities():string {
    if(sessionStorage.getItem(AUTHORITIES_KEY)!=null){
        var authorities_key = sessionStorage.getItem(AUTHORITIES_KEY);
        this.rol = JSON.parse(authorities_key!= null ? authorities_key: 'undefined' );
    }
    return this.rol;
  }

  public logOut():void {
      window.sessionStorage.clear();
  }
}
