import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Cliente } from 'src/app/model/clientes';
import { TiposIdentificacion } from 'src/app/model/tiposIdentificacion';
import { ClienteService } from 'src/app/services/cliente.service';
import { TiposidentificacionesService } from 'src/app/services/tiposidentificaciones.service';

@Component({
    templateUrl: 'dialogcliente.component.html',
    styleUrls: ['dialogcliente.component.scss']
})

export class DialogClienteComponent implements OnInit {
    
    public tipos: any;

    public id_tipo: number = 0;
    public tipo: TiposIdentificacion = {id:0,abreviatura:"",descripcion:""};
    public identificacion: string = '';
    public razon_social: string = '';
    public estado:string = '';

    constructor(
        public dialogRef: MatDialogRef<DialogClienteComponent>,
        public clienteService: ClienteService,
        public snackBar: MatSnackBar,
        private tiposService: TiposidentificacionesService,
        @Inject(MAT_DIALOG_DATA) public cliente: Cliente
    ) { 
        if(this.cliente != null){
            this.identificacion = cliente.identificacion;
            this.razon_social = cliente.razonSocial;
            this.estado = cliente.estado;
            this.tipo = cliente.idTipoIdentificacion;
            this.id_tipo= cliente.idTipoIdentificacion.id;
        }
    }

    ngOnInit() {
        this.getTipos();
     }

    cerrarDialog(){
        this.dialogRef.close();
    }

    newCliente(){
        if(this.id_tipo==0 || this.estado=='' || this.identificacion==null || this.razon_social==''){
            this.snackBar.open('POR FAVOR LLENE TODOS LOS CAMPOS REQUERIDOS','',{duration: 2000});
        }else{
            var date: Date = new Date('2021-09-19');
            var tipo_identificacion: TiposIdentificacion = {id:this.id_tipo,abreviatura:"",descripcion:""};
            var cliente: Cliente = {id:0,estado:this.estado,fechaRegistro:date,identificacion:this.identificacion,razonSocial:this.razon_social,idTipoIdentificacion: tipo_identificacion, };
            
            this.clienteService.createCliente(cliente).subscribe(response => {
                if(response.exito==1){
                    this.dialogRef.close();
                    this.snackBar.open('CLIENTE REGISTRADO CON EXITO','',{duration: 2000});
                }
                if(response.exito==-1){
                    this.snackBar.open('LA INDENTIFICACION YA SE ENCUENTRA REGISTRADA','',{duration: 2000});
                }
            });
        }
    }

    updateCliente(){
        if(this.id_tipo==0 || this.estado=='' || this.identificacion==null || this.razon_social==''){
            this.snackBar.open('POR FAVOR LLENE TODOS LOS CAMPOS REQUERIDOS','',{duration: 2000});
        }else{
            const date: Date = new Date('2021-09-19');
            const tipo_identificacion: TiposIdentificacion = {id:this.id_tipo,abreviatura:"",descripcion:""};
            const cliente: Cliente = {id:this.cliente.id,estado:this.estado,fechaRegistro:date,identificacion:this.identificacion,razonSocial:this.razon_social,idTipoIdentificacion: tipo_identificacion };

            this.clienteService.updateCliente(cliente).subscribe(response => {
                if(response.exito==1){
                    this.dialogRef.close();
                    this.snackBar.open('CLIENTE EDITADO CON EXITO','',{duration: 2000});
                }
            });
        }
    }

    getTipos(){
        this.tiposService.getTiposIdentificaiones().subscribe(response => {
            this.tipos = response.data;
        });
    }
}