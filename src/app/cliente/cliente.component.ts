import { Component, OnInit } from '@angular/core';
import { ClienteService } from '../services/cliente.service';
import { DialogClienteComponent } from "./dialog/dialogcliente.component";
import { MatDialog } from "@angular/material/dialog";
import { Cliente } from '../model/clientes';
import { TiposIdentificacion } from '../model/tiposIdentificacion';
import { DialogDeleteComponent } from '../common/delete/dialogdelete.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TiposidentificacionesService } from '../services/tiposidentificaciones.service';
import { TokenService } from '../security/services/token.service';

@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.scss']
})
export class ClienteComponent implements OnInit {

  admin: boolean = false;

  public clientes: Object[] = [];
  public tipos: TiposIdentificacion[] = [];
  public id_tipo: number = 0;
  public id_tipo_filtro: number = 0;
  public identificacion_buscada: number = 0;
  readonly width: string = "50%";
  readonly height: string = "63%";

  public columnas: string[] =  ['tipo_identificacion','identificacion','razon_social','actions'];
  constructor(
    private clienteService: ClienteService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    private tiposService: TiposidentificacionesService,
    private tokeService: TokenService
  ) { 

  }

  ngOnInit(): void {
      this.getClientes();
      this.getTipos();
      if(this.tokeService.getAuthorities()=='ROL_ADMIN'){
        this.admin = true;
      } 
  }

  limpiarFiltro(){
    this.getClientes();
    this.getTipos();
    this.id_tipo = 0;
    this.identificacion_buscada = 0;
  }

  getClientes(){
    this.clienteService.getClientes().subscribe(response => {
      this.clientes = response.data
    });
  }

  getClientesFiltrados(id_tipo_buscado:number,identificacion: number){
    if(id_tipo_buscado==0 && identificacion==0){
      this.snackBar.open("NO SE HAN APLICADO FILTROS, POR FAVOR INGRESE AL MENOS UNO DE LOS CAMPOS",'',{duration:2000});
    }else{
      this.clienteService.getClienteFiltrados(id_tipo_buscado,identificacion).subscribe(response => {
        this.clientes = response.data;
      });
    }
  }

  openNewDialog(){
    const dialogRef = this.dialog.open(DialogClienteComponent,{
      width: this.width,
      height:this.height
    });
    dialogRef.afterClosed().subscribe(response => {
      this.getClientes()
    });
  }

  openEdit(cliente:Cliente){
    const dialogRef = this.dialog.open(DialogClienteComponent,{
      width: this.width,
      height: this.height,
      data: cliente
    });
    dialogRef.afterClosed().subscribe(response => {
      this.getClientes()
    });
  }

  openDelete(id_cliente:number){
    const dialogRef = this.dialog.open(DialogDeleteComponent,{});
    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.clienteService.deleteCliente(id_cliente).subscribe(response =>{
          if(response.exito == 1){
            this.snackBar.open("CLIENTE ELIMINADO CON EXITO",'',{duration:2000});
            this.getClientes();
          }
        })
      }
    });
  }

  getTipos(){
    this.tiposService.getTiposIdentificaiones().subscribe(response => {
        this.tipos = response.data
    });
}
}