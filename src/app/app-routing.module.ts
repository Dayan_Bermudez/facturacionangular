import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { ClienteComponent } from './cliente/cliente.component';
import { FacturacionComponent } from './facturacion/facturacion.component';
import { HomeComponent } from './home/home.component';
import { ProductoComponent } from './producto/producto.component';
import { UsuarioComponent } from './usuario/usuario.component';
import { ProdGuardService as guard } from "./security/guards/prod-guard.service";


const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'cliente', component: ClienteComponent, canActivate: [guard], data: { expectedRol: ['ROL_ADMIN', 'ROL_CAJERO'] } },
  { path: 'usuario', component: UsuarioComponent, canActivate: [guard], data: { expectedRol: ['ROL_ADMIN'] } },
  { path: 'producto', component: ProductoComponent, canActivate: [guard], data: { expectedRol: ['ROL_ADMIN'] } },
  { path: 'facturacion', component: FacturacionComponent, canActivate: [guard], data: { expectedRol: ['ROL_ADMIN', 'ROL_CAJERO'] } },
  { path: 'login', component: LoginComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
