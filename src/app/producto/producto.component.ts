import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DialogDeleteComponent } from '../common/delete/dialogdelete.component';
import { Productos } from '../model/productos';
import { ProductoService } from '../services/producto.service';
import { DialogProductoComponent } from './dialog/dialogproducto.component';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.scss']
})
export class ProductoComponent implements OnInit {

  public productos: Object[]=[];
  public width:string = '60%';

  public columnas: string[] = ['id','nombre','estado','valor_unitario','actions'];

  constructor(
    private productoService: ProductoService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
      this.getProductos();
  }

  getProductos(){
    this.productoService.getProductos().subscribe(response => {
      this.productos = response.data;
    });
  }

  openNewDialog(){
    const dialogRef = this.dialog.open(DialogProductoComponent,{
      width: this.width
    });
    dialogRef.afterClosed().subscribe(response => {
      this.getProductos()
    });
  }

  openEdit(producto: Productos){
    const dialogRef = this.dialog.open(DialogProductoComponent,{
      width: this.width,
      data: producto
    });
    dialogRef.afterClosed().subscribe(response => {
      this.getProductos()
    });
  }

  openDelete(id_cliente:number){
    const dialogRef = this.dialog.open(DialogDeleteComponent,{});
    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.productoService.deleteProducto(id_cliente).subscribe(response =>{
          if(response.exito == 1){
            this.snackBar.open("PRODUCTO ELIMINADO CON EXITO",'',{duration:2000});
            this.getProductos();
          }
        })
      }
    });
  }

}
