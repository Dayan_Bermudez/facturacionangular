import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Productos } from 'src/app/model/productos';
import { ProductoService } from 'src/app/services/producto.service';

@Component({
  selector: 'app-dialogproducto',
  templateUrl: './dialogproducto.component.html',
  styleUrls: ['./dialogproducto.component.scss']
})
export class DialogProductoComponent implements OnInit {

  public id: number = 0;
  public nombre:string = '';
  public estado:string = '';
  public valor_unitario: number = 0;

  constructor(
    public dialogRef: MatDialogRef<DialogProductoComponent>,
    public productoService: ProductoService,
    public snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public producto: Productos
  ) { 
    if(this.producto != null){
      this.id = producto.id;
      this.nombre = producto.nombre;
      this.estado = producto.estado;
      this.valor_unitario = producto.valorUnitario;
    }
  }

  ngOnInit(): void {
  }

  cerrarDialog(){
    this.dialogRef.close();
  }

  newProducto(){
    if(this.nombre=='' || this.estado=='' || this.valor_unitario==0){
      this.snackBar.open('POR FAVOR LLENE TODOS LOS CAMPOS REQUERIDOS','',{duration: 2000});
    }else{
      const producto: Productos = {id:0,nombre:this.nombre,estado:this.estado,valorUnitario:this.valor_unitario};
      this.productoService.createProducto(producto).subscribe(response => {
          if(response.exito==1){
              this.dialogRef.close();
              this.snackBar.open('PRODUCTO REGISTRADO CON EXITO','',{duration: 2000});
          }
          if(response.exito==-1){
            this.snackBar.open('EL PRODUCTO YA SE ENCUENTRA REGISTRADO','',{duration: 2000});
          }
      });
    }
  }

  updateProducto(){
    if(this.nombre=='' || this.estado=='' || this.valor_unitario<=0){
      this.snackBar.open('POR FAVOR LLENE TODOS LOS CAMPOS REQUERIDOS','',{duration: 2000});
    }else{
      const producto: Productos = {id:this.id,nombre:this.nombre,estado:this.estado,valorUnitario:this.valor_unitario};
      this.productoService.updateProducto(producto).subscribe(response => {
          if(response.exito==1){
              this.dialogRef.close();
              this.snackBar.open('PRODUCTO EDITADO CON EXITO','',{duration: 2000});
          }
      });
    }
  }

}
