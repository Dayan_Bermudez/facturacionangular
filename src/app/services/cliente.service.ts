import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Cliente } from '../model/clientes';
import { Respuesta } from '../model/respuesta';

const httpOption = {
    headers: new HttpHeaders({
        'Content-Type':'application/json'
    })
}

@Injectable({
  providedIn: 'root'
})
export class ClienteService {

  url: string = 'http://localhost:8080/clientes';

  constructor(
    private _http:HttpClient
  ) { }

  getClientes():Observable<Respuesta>{
    return this._http.get<Respuesta>(this.url);
  }

  createCliente(newCliente: Cliente):Observable<Respuesta>{
    return this._http.post<Respuesta>(this.url,newCliente,httpOption);
  }

  updateCliente(upCliente: Cliente):Observable<Respuesta>{
    return this._http.put<Respuesta>(this.url,upCliente,httpOption);
  }

  deleteCliente(id: number):Observable<Respuesta>{
    return this._http.delete<Respuesta>(`${this.url}/${id}`);
  }
  //http://localhost:8080/clientes/tipoId?id_tipo_buscado=1&identificaion=543
  getClienteFiltrados(id_tipo_id:number,identificacion: number){
    return this._http.get<Respuesta>(`${this.url}/tipoId?id_tipo_buscado=${id_tipo_id}&identificacion=${identificacion}`);
  }

}
