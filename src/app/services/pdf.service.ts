import { HttpClient, HttpHeaders } from '@angular/common/http';
import { toBase64String } from '@angular/compiler/src/output/source_map';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Respuesta } from '../../app/model/respuesta';

const httpHeaders = new HttpHeaders ({
  'Content-Type'  : 'application/json'
   //'responseType'  : 'blob' as 'json'        //This also worked
});

const options = {
  headers: httpHeaders,
  responseType: 'blob' as 'json'
};

@Injectable({
  providedIn: 'root'
})
export class PdfService {

  url: string = 'http://localhost:8080/facturacion/factura/5';
  constructor(
    private _http: HttpClient
  ) { }

  getPDF():Observable<Respuesta>{
    return this._http.get<Respuesta>(this.url);
  }

}
