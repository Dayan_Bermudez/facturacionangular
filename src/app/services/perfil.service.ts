import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Respuesta } from '../model/respuesta';

@Injectable({
  providedIn: 'root'
})
export class PerfilService {

  url: string = 'http://localhost:8080/perfiles';
  constructor(
    private _http: HttpClient
  ) { }

  getPerfiles(){
    return this._http.get<Respuesta>(this.url);
  }
}
