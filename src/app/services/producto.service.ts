import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Productos } from '../model/productos';
import { Respuesta } from '../model/respuesta';

const httpOption = {
  headers: new HttpHeaders({
      'Content-Type':'application/json'
  })
}

@Injectable({
  providedIn: 'root'
})
export class ProductoService {

  url: string = 'http://localhost:8080/productos';

  constructor(
    private _http: HttpClient
  ) { }

  getProductos():Observable<Respuesta>{
    return this._http.get<Respuesta>(this.url);
  }

  getProductoId(id:number):Observable<Respuesta>{
    return this._http.get<Respuesta>(`${this.url}/${id}`);
  }

  createProducto(newProducto: Productos):Observable<Respuesta>{
    return this._http.post<Respuesta>(this.url,newProducto,httpOption);
  }

  updateProducto(upProducto: Productos):Observable<Respuesta>{
    return this._http.put<Respuesta>(this.url,upProducto,httpOption);
  }

  deleteProducto(id: number):Observable<Respuesta>{
    return this._http.delete<Respuesta>(`${this.url}/${id}`);
  }
}
