import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { NuevoUsuario } from '../model/nuevousuario';
import { Respuesta } from '../model/respuesta';
import { Usuarios } from '../model/usuario';

const httpOption = {
  headers: new HttpHeaders({
      'Content-Type':'application/json'
  })
}

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  url: string = 'http://localhost:8080/usuarios';
  constructor(
    private _http: HttpClient
  ) { }

  getUsuarios():Observable<Respuesta>{
    return this._http.get<Respuesta>(this.url);
  }

  createUsuario(newUsuario: NuevoUsuario):Observable<Respuesta>{
    return this._http.post<Respuesta>(this.url+'/nuevo',newUsuario);
  }

  updateUsuario(upUsuario: Usuarios):Observable<Respuesta>{
    return this._http.put<Respuesta>(this.url,upUsuario,httpOption);
  }

  deleteUsuario(id: number):Observable<Respuesta>{
    return this._http.delete<Respuesta>(`${this.url}/${id}`);
  }
}
