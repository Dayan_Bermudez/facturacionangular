import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Respuesta } from '../model/respuesta';

@Injectable({
  providedIn: 'root'
})
export class TiposidentificacionesService {

  url: string = 'http://localhost:8080/tipos/identificacion';

  constructor(
    private _http: HttpClient
  ) { }

  getTiposIdentificaiones():Observable<Respuesta>{
    return this._http.get<Respuesta>(this.url);
  }
}
