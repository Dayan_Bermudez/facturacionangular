import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { facturaDetalleTemporal } from '../model/facturaDetalleTemporal';
import { Respuesta } from '../model/respuesta';

const httpOption = {
  headers: new HttpHeaders({
      'Content-Type':'application/json'
  })
}

@Injectable({
  providedIn: 'root'
})
export class FacturaDetalleTemporalService {

  url: string = 'http://localhost:8080/facturaDetalleTemporal';

  constructor(
    private _http: HttpClient
  ) { }

  getFacturaDetalleTemporal():Observable<Respuesta>{
    return this._http.get<Respuesta>(this.url);
  }

  createFacturaDetalleTemporal(newFacturaDetalleT: facturaDetalleTemporal):Observable<Respuesta>{
    return this._http.post<Respuesta>(this.url,newFacturaDetalleT,httpOption);
  }

  deleteFacturaDetalleTemporalId(id: number):Observable<Respuesta>{
    return this._http.delete<Respuesta>(`${this.url}/${id}`);
  }

  deleteFacturaDetalleTemporal():Observable<Respuesta>{
    return this._http.delete<Respuesta>(this.url);
  }
}
