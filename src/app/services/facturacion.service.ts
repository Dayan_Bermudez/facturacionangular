import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Facturacion } from '../model/facturacion';
import { Respuesta } from '../model/respuesta';

const httpOption = {
  headers: new HttpHeaders({
      'Content-Type':'application/json'
  })
}

@Injectable({
  providedIn: 'root'
})
export class FacturacionService {

  url: string = 'http://localhost:8080/facturacion';

  constructor(
    private _http: HttpClient
  ) { }

  createFacturacion(newFacturacion: Facturacion):Observable<Respuesta>{
    return this._http.post<Respuesta>(this.url,newFacturacion,httpOption);
  }
}
