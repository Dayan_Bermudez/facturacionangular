import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
    templateUrl: './dialogdelete.component.html',
    styleUrls: ['./dialogdelete.component.scss']
})

export class DialogDeleteComponent implements OnInit {
    constructor(
        public dialogRef: MatDialogRef<DialogDeleteComponent>
    ) { }

    ngOnInit() { }
}