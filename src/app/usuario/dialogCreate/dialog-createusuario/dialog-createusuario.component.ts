import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NuevoUsuario } from 'src/app/model/nuevousuario';
import { Perfil } from 'src/app/model/perfil';
import { PerfilService } from 'src/app/services/perfil.service';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-dialog-createusuario',
  templateUrl: './dialog-createusuario.component.html',
  styleUrls: ['./dialog-createusuario.component.scss']
})
export class DialogCreateusuarioComponent implements OnInit {

  public idUsuario: number = 0;
  public nombre: string = '';
  public apellido: string = '';
  public usuario: string = '';
  public contrasena: string = '';

  public id_perfil: number = 0;

  public perfiles: any;

  constructor(
    public dialogRef: MatDialogRef<DialogCreateusuarioComponent>,
    public snackBar: MatSnackBar,
    private usuarioService: UsuarioService,
    private perfilService: PerfilService,
  ) { }

  ngOnInit(): void {
    this.getPerfiles();
  }

  newUsuario(){
    if(this.nombre=='' || this.usuario=='' || this.contrasena=='' || this.id_perfil==0){
      this.snackBar.open('POR FAVOR LLENE TODOS LOS CAMPOS REQUERIDOS','',{duration: 2000});
    }else{
      const perfil: Perfil = {id:this.id_perfil,nombre:""};
      const usuario: NuevoUsuario = {nombre:this.nombre,apellido: this.apellido,usuario:this.usuario,perfil:perfil,password:this.contrasena};
      this.usuarioService.createUsuario(usuario).subscribe(response => {
          if(response.exito==1){
            this.dialogRef.close();
            this.snackBar.open('USUARIO REGISTRADO CON EXITO','',{duration: 2000});
          }
          if(response.exito==-1){
            this.snackBar.open('EL NOMBRE DE USUARIO YA SE ENCUETRA REGISTRADO','',{duration: 2000});
          }
        });
    }
    
  }

  cerrarDialog(){
    this.dialogRef.close();
  }

  getPerfiles(){
    this.perfilService.getPerfiles().subscribe(response =>{
      this.perfiles = response.data
    });
  }

}
