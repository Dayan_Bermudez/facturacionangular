import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogCreateusuarioComponent } from './dialog-createusuario.component';

describe('DialogCreateusuarioComponent', () => {
  let component: DialogCreateusuarioComponent;
  let fixture: ComponentFixture<DialogCreateusuarioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogCreateusuarioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogCreateusuarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
