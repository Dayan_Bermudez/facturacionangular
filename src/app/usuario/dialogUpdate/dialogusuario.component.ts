import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Perfil } from 'src/app/model/perfil';
import { Usuarios } from 'src/app/model/usuario';
import { PerfilService } from 'src/app/services/perfil.service';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-dialogusuario',
  templateUrl: './dialogusuario.component.html',
  styleUrls: ['./dialogusuario.component.scss']
})
export class DialogusuarioComponent implements OnInit {

  public idUsuario: number = 0;
  public nombre: string = '';
  public apellido: string = '';
  public user: string = '';

  public id_perfil: number = 0;

  public perfiles: any;

  constructor(
    @Inject(MAT_DIALOG_DATA) public usuario: Usuarios,
    public dialogRef: MatDialogRef<DialogusuarioComponent>,
    public snackBar: MatSnackBar,
    private usuarioService: UsuarioService,
    private perfilService: PerfilService
  ) { 
    if(usuario!=null){
      this.idUsuario = usuario.id;
      this.nombre = usuario.nombre;
      this.apellido = usuario.apellido;
      this.user = usuario.usuario;
      this.id_perfil = usuario.perfil.id;
    }
  }

  ngOnInit(): void {
    this.getPerfiles();
  }

  cerrarDialog(){
    this.dialogRef.close();
  }

  updateUsuario(){
    if(this.nombre=='' || this.user==''  || this.id_perfil==0){
      this.snackBar.open('POR FAVOR LLENE TODOS LOS CAMPOS REQUERIDOS','',{duration: 2000});
    }else{
      const perfil: Perfil = {id:this.id_perfil,nombre:""};
      const usuario: Usuarios = {id: this.idUsuario,nombre:this.nombre,apellido: this.apellido,usuario:this.user,perfil:perfil,contrasena:""};
      this.usuarioService.updateUsuario(usuario).subscribe(response => {
          if(response.exito==1){
              this.dialogRef.close();
              this.snackBar.open('PRODUCTO EDITADO CON EXITO','',{duration: 2000});
          }
      });
    }
  }

  getPerfiles(){
    this.perfilService.getPerfiles().subscribe(response =>{
      this.perfiles = response.data
    });
  }

}
