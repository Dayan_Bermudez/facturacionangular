import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DialogusuarioComponent } from './dialogUpdate/dialogusuario.component';
import { DialogDeleteComponent } from '../common/delete/dialogdelete.component';
import { Usuarios } from '../model/usuario';
import { UsuarioService } from '../services/usuario.service';
import { DialogCreateusuarioComponent } from './dialogCreate/dialog-createusuario/dialog-createusuario.component';


@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.scss']
})
export class UsuarioComponent implements OnInit {

  width: string = '60';
  height: string = '60';

  usuarios: Object[]=[];

  public columnas: string[] =  ['id','nombre','apellido','usuario','perfil','actions'];

  constructor(
    private usuarioService: UsuarioService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
      this.getUsuarios();
  }

  getUsuarios(){
    this.usuarioService.getUsuarios().subscribe(response =>{
      this.usuarios = response.data;
    });
  }

  openNewDialog(){
    const dialogRef = this.dialog.open(DialogCreateusuarioComponent,{
      width: this.width,
      height: this.height
    });
    dialogRef.afterClosed().subscribe(response => {
      this.getUsuarios()
    });
  }

  openEdit(upUsuario: Usuarios){
    const dialogRef = this.dialog.open(DialogusuarioComponent,{
      width: this.width,
      height: this.height,
      data: upUsuario
    });
    dialogRef.afterClosed().subscribe(response => {
      this.getUsuarios()
    });
  }

  openDelete(id_usuario: number){
    const dialogRef = this.dialog.open(DialogDeleteComponent,{});
    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.usuarioService.deleteUsuario(id_usuario).subscribe(response =>{
          if(response.exito == 1){
            this.snackBar.open("USUARIO ELIMINADO CON EXITO",'',{duration:2000});
            this.getUsuarios();
          }
        })
      }
    });
  }

}
