import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TokenService } from 'src/app/security/services/token.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  isLoggedMenu = false;
  nombreUsuario: string = '';
  authoritie: string = '';
  admin: boolean = false;
  
  constructor(
    private tokenService: TokenService,
    private router: Router
  ) { }

  ngOnInit(): void {
    if(this.tokenService.getToken()!='undefined'){
      this.isLoggedMenu = true;
      this.nombreUsuario = this.tokenService.getUsername();
      this.authoritie = this.tokenService.getAuthorities();
      let auth = this.tokenService.getAuthorities();
      if(this.tokenService.getAuthorities()=='ROL_ADMIN'){
        this.admin = true;
      }
    }
  }

  logOut(){
    this.isLoggedMenu = false;
    this.nombreUsuario = '';
    this.tokenService.logOut();
    this.router.navigate(['/login']);
    window.localStorage.removeItem('clienteFacturar');
  }

}
