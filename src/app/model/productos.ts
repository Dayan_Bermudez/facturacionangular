export interface Productos{
    id: number;
    nombre: string;
    valorUnitario: number;
    estado: string;
}