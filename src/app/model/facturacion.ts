import { Factura } from "./factura";
import { FacturaDetalle } from "./facturaDetalle";
import { facturaDetalleTemporal } from "./facturaDetalleTemporal";

export interface Facturacion {
    factura: Factura;
    facturaDetalle: FacturaDetalle[];
    facturaDetalleTemporal: facturaDetalleTemporal[];
}