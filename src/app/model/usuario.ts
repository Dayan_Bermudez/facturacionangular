import { Perfil } from "./perfil";

export interface Usuarios{
    id: number;
    nombre: string;
    apellido: string;
    usuario: string;
    contrasena: string;
    perfil: Perfil;
}