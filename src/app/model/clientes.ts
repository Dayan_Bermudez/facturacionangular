import { TiposIdentificacion } from "./tiposIdentificacion";

export interface Cliente{
    id: number;
    identificacion: string;
    razonSocial: string;
    fechaRegistro: Date;
    estado: string;
    idTipoIdentificacion: TiposIdentificacion;
}