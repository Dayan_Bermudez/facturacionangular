import { Productos } from "./productos";

export interface facturaDetalleTemporal{
    id: number;
    producto: Productos;
    cantidad: number;
    total: number;
    valorUnitario: number;
}