import { Factura } from "./factura";
import { Productos } from "./productos";

export interface FacturaDetalle{
    id: number;
    factura: Factura;
    producto: Productos;
    cantidad: number;
    valorUnitario: number;
    total: number;
}