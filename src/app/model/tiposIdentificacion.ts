export interface TiposIdentificacion {
    id: number;
    abreviatura: string;
    descripcion: string;
}