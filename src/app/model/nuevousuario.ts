import { Perfil } from "./perfil";

export interface NuevoUsuario {
    nombre: string;
    apellido: string;
    usuario: string;
    password: string;
    perfil: Perfil;
}