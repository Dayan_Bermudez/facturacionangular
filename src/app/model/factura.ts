import { Cliente } from "./clientes";

export interface Factura{
    id:number;
    cliente: Cliente;
    fecha: Date;
    totalFactura: number;
}