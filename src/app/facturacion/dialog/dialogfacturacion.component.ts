import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { facturaDetalleTemporal } from 'src/app/model/facturaDetalleTemporal';
import { FacturaDetalleTemporalService } from 'src/app/services/factura-detalle-temporal.service';

@Component({
    templateUrl: 'dialogfacturacion.component.html',
    styleUrls: ['dialogfacturacion.component.scss']
})

export class DialogFacturacionComponent implements OnInit {

    constructor(
        public dialogRef: MatDialogRef<DialogFacturacionComponent>,
        public snackBar: MatSnackBar,
        @Inject(MAT_DIALOG_DATA) public facturaDetalle: facturaDetalleTemporal,
        private facturaDetalleTemporalService: FacturaDetalleTemporalService
    ) { }

    ngOnInit() { }

    cerrarDialog(){
        this.dialogRef.close();
    }

    createFacturaDetalleTemporal(){
        if(this.facturaDetalle.cantidad<=0){
            this.snackBar.open('INGRESE LA CANTIDAD DE PRODUCTOS QUE DESEA','',{duration: 2000});
        }else{
            this.facturaDetalle.total = (this.facturaDetalle.cantidad*this.facturaDetalle.producto.valorUnitario);
            this.facturaDetalleTemporalService.createFacturaDetalleTemporal(this.facturaDetalle).subscribe(
                response => {
                if(response.exito == 1){
                    this.snackBar.open('AGREGADO AL CARRITO','',{duration: 2000});
                    this.cerrarDialog();
                }});
        }
    }
}