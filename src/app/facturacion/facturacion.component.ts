import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FacturacionService } from 'src/app/services/facturacion.service';
import { ProductoService } from 'src/app/services/producto.service';
import { DialogDeleteComponent } from '../common/delete/dialogdelete.component';
import { Cliente } from '../model/clientes';
import { Factura } from '../model/factura';
import { Facturacion } from '../model/facturacion';
import { facturaDetalleTemporal } from '../model/facturaDetalleTemporal';
import { Productos } from '../model/productos';
import { TiposIdentificacion } from '../model/tiposIdentificacion';
import { ClienteService } from '../services/cliente.service';
import { FacturaDetalleTemporalService } from '../services/factura-detalle-temporal.service';
import { TiposidentificacionesService } from '../services/tiposidentificaciones.service';
import { DialogFacturacionComponent } from './dialog/dialogfacturacion.component';


@Component({
  selector: 'app-facturacion',
  templateUrl: './facturacion.component.html',
  styleUrls: ['./facturacion.component.scss']
})
export class FacturacionComponent implements OnInit {

  public idProductoFiltrado: number = 0;

  public panelOpenStateCliente = false;
  public panelOpenStateCarrito = false;

  public width:string = '25';
  public height:string = '25';

  public total_Facturado: number = 0;
  

  public cliente_a_facturar: any;
  public clientes: Object[] = [];
  public productos: Object[] = [];
  public facturasDetalle:facturaDetalleTemporal[]=[];

  public tipos: TiposIdentificacion[] = [];
  public identificacion_buscada: number = 0;
  public id_tipo_filtro: number = 0;

  public columnas: string[] = ['id','nombre','valor_unitario','actions'];
  public carritoColumnas: string[] = ['id','nombre','valor_unitario','unidades','total','actions'];

  public colums: string[] =  ['tipo_identificacion','identificacion','razon_social','actions'];

  private clienteFacturacionKey: string = 'clienteFacturar';

  constructor(
    private facturacionService: FacturacionService,
    private productoService: ProductoService,
    private dialog: MatDialog,
    private facturaDetalleTemporalService: FacturaDetalleTemporalService,
    public snackBar: MatSnackBar,
    private tiposService: TiposidentificacionesService,
    private clienteService: ClienteService
  ) { }

  ngOnInit(): void {
      this.getProductos();
      this.getFacturaDetalleTemporal();
      this.getTipos();
      if(window.localStorage.getItem(this.clienteFacturacionKey)!=null){
        let cliente = window.localStorage.getItem(this.clienteFacturacionKey);
        this.cliente_a_facturar = (cliente != null ? JSON.parse(cliente) : null);
      }else{
        window.localStorage.removeItem(this.clienteFacturacionKey);
      }
  }

  limpiarCarrito(){
    this.facturaDetalleTemporalService.deleteFacturaDetalleTemporal().subscribe(response =>{
      if(response.exito==1){
        this.snackBar.open('CARRITO VACIO','',{duration: 2000});
        this.getFacturaDetalleTemporal();
      }
    });
  }

  facturar(){
    if(this.cliente_a_facturar!=null && this.facturasDetalle.length != 0){
      var fecha: Date = new Date;
      var factura: Factura = {id:0,cliente:this.cliente_a_facturar,fecha:fecha,totalFactura:0};
      var facturacion: Facturacion = {facturaDetalleTemporal:this.facturasDetalle,factura:factura,facturaDetalle:[]};
      this.facturacionService.createFacturacion(facturacion).subscribe(response =>{
        if(response.exito == 1){
          this.cliente_a_facturar = null;
          this.total_Facturado = 0;
          this.facturasDetalle = [];
          this.clientes = [];
          window.localStorage.removeItem(this.clienteFacturacionKey);
          this.snackBar.open('FACTURACION EXITOSA','',{duration: 2000});

          let documento = document.createElement('object'); 
          documento.style.width = '100%';
          documento.style.height = '842pt';
          documento.type = 'application/pdf';
          documento.data = 'data:application/pdf;base64,' + [response.data];
          document.body.appendChild(documento);

        }
      }); 
    }
    if(this.cliente_a_facturar==null){
      this.snackBar.open('NO SE PUDO FACTURAR, ELIGA UN CLIENTE','',{duration: 2000});
    }
    if(this.facturasDetalle.length == 0){
      this.snackBar.open('NO SE PUDO FACTURAR, AGREGUE PRODUCTOS AL CARRITO','',{duration: 2000});
    }
  }

  getProductos(){
    this.productoService.getProductos().subscribe(response => {
      this.productos = response.data;
    });
  }

  getFacturaDetalleTemporal(){
    this.total_Facturado = 0;
    this.facturaDetalleTemporalService.getFacturaDetalleTemporal().subscribe(response => {
      this.facturasDetalle = response.data;
      this.facturasDetalle.forEach(element => {
        this.total_Facturado += element.total;
      });
    });
  }

  addCarrito(productoAdd: Productos){
    var facturaDetalle: facturaDetalleTemporal = {producto:productoAdd,cantidad:0,id:0,total:0,valorUnitario:productoAdd.valorUnitario};
    this.openDialog(facturaDetalle);
  }

    deleteCarrito(idFacturaDetalle: number){
      const dialogRef = this.dialog.open(DialogDeleteComponent,{});
      dialogRef.afterClosed().subscribe(result => {
        if(result){
          this.facturaDetalleTemporalService.deleteFacturaDetalleTemporalId(idFacturaDetalle).subscribe(
            response => {
             if(response.exito == 1){
               this.getFacturaDetalleTemporal();
               this.snackBar.open('PRODUCTO ELIMINADO DEL CARRITO','',{duration: 2000});
             }
            });
        }
      });
  }

  openDialog(facturaDetalleAdd: facturaDetalleTemporal){
    const dialogRef = this.dialog.open(DialogFacturacionComponent,{
      width: this.width,
      height:this.height,
      data:facturaDetalleAdd
    });
    dialogRef.afterClosed().subscribe(response => {
      this.getFacturaDetalleTemporal();
      this.getProductos();
    });
  }

  getTipos(){
    this.tiposService.getTiposIdentificaiones().subscribe(response => {
        this.tipos = response.data
    });
  }

  limpiarFiltroCliente(){
    this.getTipos();
    this.id_tipo_filtro = 0;
    this.identificacion_buscada = 0;
    this.clientes = [];
  }

  getClientesFiltrados(id_tipo_buscado:number,identificacion: number){
    if(identificacion==null){
      identificacion = 0 ;
    }
    if(id_tipo_buscado==0 && identificacion==0){
      this.snackBar.open("NO SE HAN APLICADO FILTROS, POR FAVOR INGRESE AL MENOS UNO DE LOS CAMPOS",'',{duration:2000});
    }else{
      this.clienteService.getClienteFiltrados(id_tipo_buscado,identificacion).subscribe(response => {
        this.clientes = response.data;
      });
    }
  }

  Seleccionar(clienteSeleccionado: Cliente){
      window.localStorage.setItem(this.clienteFacturacionKey,JSON.stringify(clienteSeleccionado));
      this.cliente_a_facturar = clienteSeleccionado;
      this.panelOpenStateCliente = false;
      this.clientes = [];
  }

  limpiarFiltroProducto(){
    this.getProductos();
    this.idProductoFiltrado = 0;
  }

  getProductosFiltrados(idProductoFiltrado: number){
    if(idProductoFiltrado != 0 && idProductoFiltrado != null){
      this.productoService.getProductoId(idProductoFiltrado).subscribe(response => {
        if(response.exito != 0){
          this.productos = response.data
        }else{
          this.snackBar.open("EL CODIGO INGRESADO NO CORRSPONDE A NINGUN PRODUCTO",'',{duration:2000});
        }
      });
    }else{
      this.snackBar.open("INGRESE UN CODIGO",'',{duration:2000});
    }
  }
}
